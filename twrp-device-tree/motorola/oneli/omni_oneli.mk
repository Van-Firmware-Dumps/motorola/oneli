#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from oneli device
$(call inherit-product, device/motorola/oneli/device.mk)

PRODUCT_DEVICE := oneli
PRODUCT_NAME := omni_oneli
PRODUCT_BRAND := motorola
PRODUCT_MODEL := Taro for arm64
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="oneli-user 12 U2SLS34.1-42-14-4 c38961 release-keys"

BUILD_FINGERPRINT := motorola/oneli/oneli:12/U2SLS34.1-42-14-4/c38961:user/release-keys
